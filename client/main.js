import { Template } from 'meteor/templating';
import { Notes } from '../lib/collections.js';
import '../lib/locale.js';
// pour installer moment.js (pour voir les dates en format date...)
// meteor add momentjs:moment
import { moment } from 'meteor/momentjs:moment';


import './main.html';

Template.registerHelper('formatDate', function(date, format) {
  // Use moment.js formatting to return a date we like (http://momentjs.com/)
  // Example: {{formatDate createdAt 'MM/DD/YYYY'}}

  return moment(new Date(date)).format(format);
});




Template.body.helpers({
  /* notes: [
    {text: 'Ma note 1'},
    {text: 'Ma note 2'},
    {text: 'Ma note 3'}
  ]*/
  notes(){
    return Notes.find({});
  }


});
